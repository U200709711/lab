import java.io.IOException;
import java.util.Scanner;

public class TicTacToe {

	public static void main(String[] args) throws IOException {
		Scanner reader = new Scanner(System.in);
		char[][] board = { { ' ', ' ', ' ' }, { ' ', ' ', ' ' }, { ' ', ' ', ' ' } };

		printBoard(board);



		int movecount=0;
		int currentplayer = 0;
		while (movecount<9){
			System.out.println("Player " + (currentplayer + 1) + " enter row number:");
			int row = reader.nextInt();
			System.out.print("Player " + (currentplayer + 1) + " enter column number:");
			int col = reader.nextInt();
			if (row>0 && row<4 && col>0 && col<4 && board[row - 1][col - 1] == ' ' ) {
				if (currentplayer == 0)
					board[row - 1][col - 1] = 'X';
				else
					board[row - 1][col - 1] = '0';
				printBoard(board);
				boolean win =checkBoard(board);
				if(win){
					System.out.println("Player " + (currentplayer + 1) + " has won the game!");
					break;
				}
				movecount++;
				currentplayer = (currentplayer + 1) % 2;// osilates between 0 and 1
			}
			else
				System.out.println("The move is not valid!");
		}
		if(movecount==9)
		System.out.println("the game has ended with a draw");
		reader.close();
	}

	public static boolean checkBoard(char[][] board) {
		//check all the rows
		for(int row = 0;row < 3;row++) {
			if ((board[row][0] != ' ') && (board[row][0] == board[row][1]) && (board[row][1] == board[row][2]))
				return true;
		}
		//check all the columns
		for(int col = 0;col < 3;col++) {
			if ((board[0][col] != ' ') && (board[0][col] == board[1][col]) && (board[1][col] == board[2][col]))
				return true;
		}
		//check all the top-left to bottom-right
		if(board[0][0] != ' ' && (board[0][0] == board[1][1]) && (board[1][1] == board[2][2]))
			return true;
		//check all bottom-left to top-right
		if(board[2][0] != ' ' && (board[2][0] == board[1][1]) && (board[1][1] == board[0][2]))
			return true;
		//return false
		return false;
	}

	public static void printBoard(char[][] board) {
		System.out.println("    1   2   3");
		System.out.println("   -----------");
		for (int row = 0; row < 3; ++row) {
			System.out.print(row + 1 + " ");
			for (int col = 0; col < 3; ++col) {
				System.out.print("|");
				System.out.print(" " + board[row][col] + " ");
				if (col == 2)
					System.out.print("|");

			}
			System.out.println();
			System.out.println("   -----------");

		}

	}

}