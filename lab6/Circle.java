public class Circle {
    int radius;
    Point Center;

    public Circle(int radius, Point center) {
        this.radius = radius;
        Center = center;
    }
    public double area(){
        return Math.PI * Math.pow(radius,2);
    }
    public double perimeter(){
        return Math.PI * 2 * radius;
    }
    public boolean intersect(Circle c){
        // if (rad1 + rad2) >= dist(center1, center2)
        return (radius + c.radius) >= Center.distanceFromAnotherPoint(c.Center);
    }
}
