public class Point {
    public int xCoord;
    public int yCoord;

    public Point(int xy){
        xCoord = xy;
        yCoord = xy;
    }
    public Point(int x,int y){
        xCoord = x;
        yCoord = y;
    }
    public double distanceFromAnotherPoint(Point p){
        return Math.sqrt(Math.pow( xCoord- p.xCoord , 2) + Math.pow( yCoord- p.yCoord , 2));
    }
}
