public class Main {
    public static void main(String[] args) {
        Point tl1 = new Point(5, 7);
        System.out.println("tl1 is located at " + tl1.xCoord + ", " + tl1.yCoord);
        Rectangle rect1 = new Rectangle(10,6, tl1);
        System.out.println("The area of rec1 = " + rect1.area() + " and the perimeter of rect1 = " + rect1.perimeter());

        Point[] rect1Corners = rect1.corners();

        for(int i = 0; i <rect1Corners.length; i++){
            System.out.println("The " + (i+1) + "th corner of rect1 is located at " + rect1Corners[i].xCoord + ", " + rect1Corners[i].yCoord);
        }
        Circle c1 = new Circle(10, new Point(0,0));
        System.out.println("The area of c1 = " + c1.area() + " and the perimeter of c1 = " + c1.perimeter());
        Circle c2 = new Circle(5, new Point(7));
        boolean isIntersect = c1.intersect(c2);
        if(isIntersect)
            System.out.println("C1 and c2 are intersecting");
        else System.out.println("C1 and c2 are not intersecting");
    }
}
