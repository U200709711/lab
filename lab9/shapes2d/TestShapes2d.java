package shapes2d;

public class TestShapes2d {
    public static void main(String[] args) {
        Circle c = new Circle(5.0);
        System.out.println(c);
        System.out.println("Area of the Circle: " + c.area());

        Square s = new Square(5.0);
        System.out.println(s);
        System.out.println("Area of the square: " + s.area());
    }
}
