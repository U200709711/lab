package shapes2d;

public class Square {
    public double getSide() {
        return side;
    }

    private double side;

    public Square(double side) {
        this.side = side;
    }
    public double area(){
        return Math.pow(side,2);
    }

    @Override
    public String toString() {
        return
                "side=" + side;
    }
}
